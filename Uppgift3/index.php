<?php

require_once("./src/controller/Application.php");
require_once("./src/view/HTMLPage.php");

$mysqli = new mysqli("mysql08.citynetwork.se", 
					 "119294-ap62823", 
					 "Password123", 
					 "119294-labbar");

session_start();

$application = new \controller\Application($mysqli);
$content = $application->doApplication();

$titel = $application->getTitel();

$pageView = new \view\HTMLPage();
echo $pageView->getPage($titel, $content);

$mysqli->close();