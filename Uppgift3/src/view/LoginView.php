<?php

namespace view;

require_once("./src/model/iMessageSetter.php");

class LoginView implements \model\iMessageSetter{

	/**
	 * Location for $_POST where the username exists.
	 * @var String  
	 */
	private static $UserName = "Login::UserName";

	/**
	 * Location for $_POST where the password exists.
	 * @var String  
	 */
	private static $Password = "Login::Password";

	/**
	 * Location for $_POST where the value for if the user 
	 * should be saved exists.
	 * @var String
	 */  
	private static $SaveUser = "Login::SaveUser";

	/**
	 * Location for $_GET when the user tries to login
	 * @var String
	 */
	private static $login = "login";

	/**
	 * Location for $_GET when the user tries to logout
	 * @var String
	 */
	private static $logout = "logout";
	
	/** 
	 * Feedback from input
	 * @var String
	 */
	private $feedBackMessage = "";

	/**
	 * Location for cookie where the username exists.
	 * @var String
	 */
	private static $UserCookieLocation = "LoginView::UserName";

	/**
	 * Location for cookie where the password exists.
	 * @var String
	 */	
	private static $PassCookieLocation = "LoginView::Password";

	/**
	 * @return String HTML
	 */ 
	public function getLoggedinHTML($userName){

		$date = $this->getDate();
		return "<h1>Laborationskod fj222dr</h1>
				<h2>$userName är inloggad</h2>
				<p>$this->feedBackMessage</p>
				<a href='?logout'>Logga ut</a>
				<p>$date</p>";
	}

	/**
	 * Login form
	 * @return String HTML
	 */
	public function getLoginForm(){	
		$userName = $this->getInput(self::$UserName);
		$date = $this->getDate();
		$form = "<form method='post' action='?" . self::$login . 
					"' enctype='multipart/form-data' >
					<fieldset>
						<legend>
							Login - Skriv in användarnamn och lösenord
						</legend>
						<p>$this->feedBackMessage</p>
						<label>Användarnamn:</label>
						<input type='text' name='". self::$UserName .
							"' id='UserNameID' value='$userName' size='20'/>
						<label>Lösenord:</label>
						<input type='password' name='". self::$Password .
							"' id='PasswordID' size='20'/>
						<label>Håll mig inloggad:</label>
						<input type='checkbox' name='". self::$SaveUser .
							"' id='SaveUserID' value='1' />
						<input type='submit' value='Logga in' /> 
					</fieldset>
				</form>";			
		return "<h1>Laborationskod fj222dr</h1>
				<h2>Ej Inloggad</h2>
				$form
				<p>$date</p>";
	}

	/**
	 * @param  String $input Location for $_POST
	 * @return String Input
	 */
	private function getInput($input){
		if(isset($_POST[$input])){
			return $this->sanitize($_POST[$input]);
		}else{
			return "";
		}
	}

	/**
	 * Sanitize the input
	 * @param  String $input 
	 * @return String 
	 */
	private function sanitize($input){
		$sanitize = trim($input);
		return filter_var($sanitize, FILTER_SANITIZE_STRING);
	}

	/**
	 * Returns user created from input
	 * @return \model\UserCredentials
	 */
	public function getInputUserCredentials(){
		$userName = $this->getInput(self::$UserName);
		$password = $this->getInput(self::$Password);

		$saveUser;
		if($this->getInput(self::$SaveUser) == 1){
			$saveUser = true;
		}else{
			$saveUser = false;
		}

		$userCredentials = new \model\UserCredentials(
												$userName, 
												$password, 
												$saveUser, 
												$_SERVER['HTTP_USER_AGENT'], 
												$_SERVER['REMOTE_ADDR'], 
												0, 
												false, 
												$this);	

		return $userCredentials;
	}

	/**
	 * Returns user created from cookie
	 * @throws  Exception If there is no cookie
	 * @return \model\UserCredentials
	 */
	public function getCookieUserCredentials(){

		if(!$this->hasCookies()){
			throw new \Exception();
		}

		$userName = $_COOKIE[self::$UserCookieLocation];
		$password = $_COOKIE[self::$PassCookieLocation]; 

		$userCredentials = new \model\UserCredentials(
												$userName, 
												$password, 
												true, 
												$_SERVER['HTTP_USER_AGENT'], 
												$_SERVER['REMOTE_ADDR'], 
												0, 
												true, 
												$this);	

		return $userCredentials;
	}

	/**
	 * @param String $userName
	 * @param String $temporaryPassword
	 * @param Integer $time
	 */
	public function setCookies($userName, $temporaryPassword, $time){

		$this->removeCookies();

		setcookie(self::$UserCookieLocation, $userName, $time);
		setcookie(self::$PassCookieLocation, $temporaryPassword, $time);	
	}

	public function removeCookies(){

		if(isset($_COOKIE[self::$UserCookieLocation])){
			setcookie(self::$UserCookieLocation, "", time()-10000);
		}
		if(isset($_COOKIE[self::$PassCookieLocation])){
			setcookie(self::$PassCookieLocation, "", time()-10000);
		}		
	}

	/**
	 * @return Boolean
	 */
	private function hasCookies(){
		return isset($_COOKIE[self::$UserCookieLocation]) && 
			   isset($_COOKIE[self::$PassCookieLocation]);
	}

	/**
	 * Returns if user tries to login
	 * @return boolean
	 */
	public function userLogsin(){
		return isset($_GET[self::$login]);		
	}

	/**
	 * Returns if user wants to logout
	 * @return boolean
	 */
	public function userLogsout(){
		return isset($_GET[self::$logout]);		
	}

	/**
	 * @return String
	 */ 
	public function getUserAgent(){
		return $_SERVER['HTTP_USER_AGENT'];
	}

	/**
	 * @return String
	 */ 
	public function getIp(){
		return $_SERVER['REMOTE_ADDR'];
	}

	public function userNameError(){

		$this->feedBackMessage = "Användarnamn saknas";
	}

	public function passwordError(){

		$this->feedBackMessage = "Lösenord saknas";
	}

	public function incorrectInput(){

		$this->feedBackMessage = "Felaktigt användarnamn och/eller lösenord";
	}

	public function loginSuccess(){

		$this->feedBackMessage = "Inloggning lyckades";
	}

	public function logoutSuccess(){

		$this->feedBackMessage = "Du har nu loggat ut";
	}

	public function cookieSaved(){

		$this->feedBackMessage = 
			"Inloggning lyckades och vi kommer ihåg dig nästa gång";
	}

	public function cookieLogin(){

		$this->feedBackMessage = "Inloggning lyckades via cookies";
	}

	public function cookieError(){

		$this->feedBackMessage = "Felaktig information i cookie";
	}

	/**
	 * @return String
	 */ 
	public function signedInTitel(){
		return "Laboration. Inloggad";
	}	

	/**
	 * @return String
	 */ 
	public function signedOutTitel(){
		return "Laboration. Utloggad";
	}	


	/**
	 * Weekdays in Swedish
	 * @var array
	 */
	private $day = array("Måndag", "Tisdag", "Onsdag", "Torsdag", "Fredag", 
		"Lördag", "Söndag");

	/**
	 * Months in Swedish
	 * @var array
	 */
	private $month = array("Januari", "Februari", "Mars", "April", "Maj","Juni",
	 	"Juli", "Augusti", "September", "Oktober", "November", "December");

	/**
	 * @return String HTML
	 */
	public function getDate(){

		return "<p>" . 
			$this->day[date("N") - 1] . ", den " . date("j") . " " . 
			$this->month[date("n") - 1] . " år " . date("Y") . 
			". Klockan är [" . date("G") . ":" . date("i") . ":" . date("s") . 
			"].</p>";
	}
}