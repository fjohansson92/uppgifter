<?php

namespace model;

class LoginModel{

	/**
	 * @param \model\UserSaver  
	 */ 
	private $userSaver;

	/**
	 * @param \model\UserCredentials  
	 */ 
	private $cookieUser;

	/**
	 * @param String
	 */ 
	private $userAgent;

	/**
	 * @param Integer
	 */ 	
	private $ip;

	/**
	 * @param \model\UserCredentials 
	 */ 
	private $userCredentials;

	/**
	 * @param \model\UserDAL
	 */ 
	private $userDAL;

	/**
	 * @param \model\iMessageSetter Interface 
	 */ 
	private $iMessageSetter;

	/**
	 * @param Boolean
	 */ 
	private $generateNewPass = false;

	private $ownSession = true;

	/**
	 * @param UserSaver $userSaver
	 * @param String $userAgent
	 * @param Integer $ip
	 * @param UserDAL $userDAL
	 * @param iMessageSetter $iMessageSetter
	 */
	public function __construct(UserSaver 		$userSaver, 
												$userAgent, 
												$ip, 
												$userDAL,
								iMessageSetter 	$iMessageSetter){

		$this->userSaver = $userSaver;
		$this->userAgent = $userAgent;
		$this->ip = $ip;
		$this->userDAL = $userDAL;
		$this->iMessageSetter = $iMessageSetter;
	}

	/**
	 * @return \model\UserCredentials 	
	 */	
	public function getUserCredentials(){
		return $this->userCredentials;
	}

	/**
	 * @param Boolean $newLogin 
	 * @return Boolean User is loggedin	
	 */
	public function isLoggedin($newLogin){

		try{		
			return $this->sessionLogin($newLogin);
		}catch(\Exception $exception){}

		try{
			return $this->cookieLogin();
		}catch(\Exception $exception){
			$this->iMessageSetter->cookieError();
		}

		return false;
	}

	/**
	 * @param Boolean $newLogin
	 * @return String User is loggedin
	 */
	private function sessionLogin($newLogin){
		
		$sessionUser = $this->userSaver->getUser();
		
		if($this->authorizeSession($sessionUser)){
			$this->userCredentials = $sessionUser;
			
			if($newLogin){
				if($sessionUser->getSaveUser()){
					$this->iMessageSetter->cookieSaved();
				}else{
					$this->iMessageSetter->loginSuccess();
				}
			}
			return true;	
		}
		if($newLogin){
			if($this->ownSession){
				$this->iMessageSetter->incorrectInput();
				$this->userSaver->removeUser();
			}
		}
		return false;
	}

	/**
	 * @throws  Exception If cookie is invalid.
	 * @return String User is loggedin	
	 */
	private function cookieLogin(){

		if(isset($this->cookieUser)){
			$userName = $this->cookieUser->getUserName();
			$password = $this->cookieUser->getPassword();

			$userCredentials = $this->userDAL->getAccount(
													  $userName, 
													  $password,
													  $this->iMessageSetter);

			if($this->authorize($userCredentials) && 
			   $userCredentials->getTime() > time()){

				$this->iMessageSetter->cookieLogin();
				$this->userSaver->setUser($userCredentials);
				$this->generateNewPass = true;
				$this->userCredentials = $userCredentials;
				return true;	
			}
			throw new \Exception();
		}
	}

	public function logout(){
		try{
			$this->iMessageSetter->logoutSuccess();
			$this->userSaver->removeUser();

		}catch(\Exception $exception){}
	}

	/**
	 * @param \model\UserCredentials $cookieUser 	
	 */
	public function setUser(UserCredentials $cookieUser){

		$this->cookieUser = $cookieUser;
	}

	/**
	 * @param \model\UserCredentials $userCredentials 
	 * @return Boolean 
	 */
	private function authorize(UserCredentials $userCredentials){

		$ownSession = $userCredentials->getIp() == $this->ip &&
			  		  $userCredentials->getUserAgent() == $this->userAgent;
		$this->ownSession = $ownSession;

		return $userCredentials->getUserName() == "Admin" && $ownSession; 
	}

	/**
	 * @param \model\UserCredentials $userCredentials 
	 * @return Boolean
	 */
	private function authorizeSession(UserCredentials $userCredentials){

		return ($userCredentials->getPassword() == "Password" ||
				$userCredentials->getRecreated()) &&
				$this->authorize($userCredentials) ;
	} 

	/**
	 * @return Boolean
	 */
	public function generateNewPass(){

		return $this->generateNewPass;
	}

	/**
	 * @return String Random	
	 */
	public function getRandomString(){

		return md5(mcrypt_create_iv(32));
	}

	/**
	 * @return Integer	
	 */
	public function getEndTime(){

		return time()+100;
	}
}