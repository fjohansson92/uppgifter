<?php

namespace model;

class UserCredentials{

	/**
	 * @param String
	 */ 
	private $userName;

	/**
	 * @param Boolean
	 */ 
	private $saveUser;

	/**
	 * @param String
	 */ 
	private $userAgent;

	/**
	 * @param String
	 */ 
	private $ip;

	/**
	 * @param Integer
	 */ 
	private $time;

	/**
	 * @param String
	 */ 
	private $password;

	/**
	 * @param Boolean
	 */ 
	private $recreated;

	/**
	 * @param String $userName
	 * @param String $password
	 * @param Boolean $saveUser
	 * @param String $userAgent
	 * @param String $ip
	 * @param Integer $time
	 * @param Boolean $recreated
	 * @param iMessageSetter $iMessageSetter
	 * @throws  Exception 
	 */
	public function __construct($userName,	$password,	$saveUser,
								$userAgent,	$ip,		$time, 
								$recreated, iMessageSetter $messageSetter){
		if($userName == ""){
			$messageSetter->userNameError();
			throw new \Exception();
		}else if($password == ""){
			$messageSetter->passwordError();
			throw new \Exception();
		}else if($ip == "" ||  $userAgent == ""){
			throw new \Exception();
		}
		$this->userName = $userName;
		$this->saveUser = $saveUser;
		$this->userAgent = $userAgent;
		$this->ip = $ip;
		$this->time = $time;
		$this->password = $password;
		$this->recreated = $recreated;
	}

	/** 
	 * @return String	
	 */	
	public function getUserName(){
		return $this->userName;		
	}

	/** 
	 * @return String	
	 */
	public function getUserAgent(){
		return $this->userAgent;		
	}

	/** 
	 * @return String	
	 */
	public function getIp(){
		return $this->ip;		
	}

	/** 
	 * @return Boolean	
	 */
	public function getSaveUser(){
		return $this->saveUser;		
	}

	/** 
	 * @return String	
	 */
	public function getTime(){
		return $this->time;		
	}

	/** 
	 * @return String	
	 */
	public function getPassword(){
		return $this->password;
	}

	/** 
	 * @return Boolean	
	 */
	public function getRecreated(){
		return $this->recreated;
	}

}

