<?php

namespace model;

class UserDAL{

	/**
	 * @var String 
	 */
	private static $tableName = "accounts";

	/**
	 * @var \mysqli
	 */ 
	private $mysqli;

	/**
	 * @param \mysqli $mysqli 
	 */ 
	public function __construct(\mysqli $mysqli){

		$this->mysqli = $mysqli;
	}

	/**
	 * Get saved UserCredentials from database.
	 * @param  String $userName 
	 * @param  String $password
	 * @param  \model\iMessageSetter $iMessageSetter
	 * @throws  Exception If account couldn't be found
	 * @return \model\UserCredentials 
	 */  
	public function getAccount(						 $userName, 
							   						 $password, 
							   \model\iMessageSetter $iMessageSetter){	
		$sql = "SELECT userName, agent, time, password, ip
				FROM " . self::$tableName . "
				WHERE userName = '" . $userName . "' AND
					  password = '" . $password . "';";
		$result = $this->testMysqli($sql);  
		$account;
		while ($object = $result->fetch_array(MYSQLI_ASSOC)){
			$account = new UserCredentials($object["userName"],
								   $object["password"],
								   true,
								   $object["agent"],
								   $object["ip"],
								   $object["time"],
								   true,
								   $iMessageSetter);
		}
		if(isset($account))
			return $account;
		throw new \Exception();
	}

	/**
	 * Save account in database.
	 * @param \model\UserCredentials $userCredentials
	 * @param String $tempPassword
	 * @param Integer $time
	 * @throws  Exception If account couldn't be saved
	 */ 
	public function saveAuthorization(UserCredentials $userCredentials, 
													  $tempPassword, 
													  $time){
		$sql = "INSERT INTO " . self::$tableName . "
				(userName, agent, time,	password, ip)
				VALUES(?, ?, ?, ?, ?)";
		$statement = $this->mysqli->prepare($sql);
		if($statement === FALSE){
			throw new \Exception();
		}
		if ($statement->bind_param("sssss",$userCredentials->getUserName(),			
										   $userCredentials->getUserAgent(),
										   $time,
										   $tempPassword,
										   $userCredentials->getIp()) 
										   === FALSE){
			throw new \Exception();
		}
		if ($statement->execute() === FALSE) {
			throw new \Exception();
		}
	}	

	/**
	 * Remove userCredentials from database and invalid ones.
	 * @param  String $userName
	 * @param  String $password
	 * @throws  Exception If account couldn't be removed from database
	 */  
	public function cleanDatabase($userName, $password){

		$sql = "DELETE FROM " . self::$tableName . "
				WHERE userName = '" . $userName . "' AND  
					  password = '" . $password . "' OR
					  time < '" . time() . "';"; 

		if ($this->mysqli->query($sql) === FALSE) {
			throw new \Exception();
		}
	}

	/**
	 * @param  String $sql 
	 * @throws  Exception if something is wrong with mysqli
	 * @return \mysqli_result 
	 */ 
	private function testMysqli($sql){

		$statement = $this->mysqli->prepare($sql);
		if($statement === FALSE){
			throw new \Exception();
		}

		if ($statement->execute() === FALSE) {
			throw new \Exception();
		}

		return $statement->get_result();
	}
}