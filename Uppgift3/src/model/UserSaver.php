<?php

namespace model;

class UserSaver{

	/**
	 * Location for $_SESSION where the user exists.
	 * @var string
	 */
	private static $UserSessionLocation = "model::UserSaver::User";

	public function __construct(){
		assert(isset($_SESSION));
	}	

	/**
	 * Returns the user if there is one saved in session
	 * @throws  Exception If session not set
	 * @return UserCredentials
	 */
	public function getUser(){
		if (isset($_SESSION[self::$UserSessionLocation])) {
			return $_SESSION[self::$UserSessionLocation];
		}
		throw new \Exception;
	}

	/**
	 * Sets the session
	 * @param \model\UserCredentials $user
	 */
	public function setUser(UserCredentials $user){
		$_SESSION[self::$UserSessionLocation] = $user;
	}

	/**
	 * Removes the session
	 */
	public function removeUser(){
		session_unset(self::$UserSessionLocation);
	}
}
