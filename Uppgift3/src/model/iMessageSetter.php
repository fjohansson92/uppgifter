<?php

namespace model;

interface iMessageSetter{
	public function userNameError();
	public function passwordError();
	public function incorrectInput();
	public function loginSuccess();
	public function logoutSuccess();
	public function cookieSaved();
	public function cookieLogin();
	public function cookieError();
}