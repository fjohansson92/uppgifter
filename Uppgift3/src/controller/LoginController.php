<?php

namespace controller;

require_once("./src/model/UserCredentials.php");

class LoginController{

	/**
	 * @param \view\LoginView  
	 */ 
	private $loginView;

	/**
	 * @param \model\UserSaver 
	 */ 
	private $userSaver;

	/**
	 * @param \model\UserDAL 
	 */ 
	private $userDAL;

	/**
	 * @param \model\LoginModel
	 */ 
	private $loginModel;

	/**
	 * @param \view\LoginView $loginView
	 * @param \model\UserSaver $userSaver
	 * @param \model\UserDAL $userDAL
	 * @param \model\LoginModel $loginModel
	 */
	public function __construct(\view\LoginView 	$loginView, 
								\model\UserSaver 	$userSaver, 
								\model\UserDAL 		$userDAL,
								\model\LoginModel 	$loginModel){

		$this->loginView = $loginView;
		$this->userSaver = $userSaver;
		$this->userDAL = $userDAL;
		$this->loginModel = $loginModel;
	}

	/**
	 * Saves user input
	 */ 
	public function login(){

		$this->loginView->removeCookies();

		if($this->loginView->userLogsin()){
			try{

				$userCredentials = $this->loginView->getInputUserCredentials();
				$this->userSaver->setUser($userCredentials);
			}catch(\Exception $exception){}
		}
	}

	/**
	 * @param \model\UserCredentials $userCredentials 
	 * @param Boolean $generateNewPass
	 * @return String HTML	
	 */
	public function stayLoggedin(\model\UserCredentials $userCredentials, 
														$generateNewPass){

		if($userCredentials->getSaveUser() && $generateNewPass){

			$endTime = $this->loginModel->getEndTime();
			$temporaryPassword = $this->loginModel->getRandomString();

			try{
				$this->userDAL->saveAuthorization($userCredentials, 
												  $temporaryPassword, 
												  $endTime);

				$this->loginView->setCookies($userCredentials->getUserName(),
											 $temporaryPassword, 
											 $endTime);
			}catch(\Exception $exception){}
		}

		$userName = $userCredentials->getUserName();
		return $this->loginView->getLoggedinHTML($userName);
	}

	/**
	 * @param \model\UserCredentials $userCredentials
	 * @return String HTML	
	 */
	public function logout(\model\UserCredentials $userCredentials){

		$this->loginModel->logout();
		$this->loginView->removeCookies();

		$userName = $userCredentials->getUserName();
		$password = $userCredentials->getPassword();

		$this->userDAL->cleanDatabase($userName, $password);

		return $this->loginView->getLoginForm();
	}
}