<?php

namespace controller;

require_once("./src/controller/LoginController.php");
require_once("./src/model/UserSaver.php");
require_once("./src/view/LoginView.php");
require_once("./src/model/LoginModel.php");
require_once("./src/model/UserDAL.php");
require_once("./src/model/iMessageSetter.php");

class Application{

	/**
	 * @param \view\LoginView  
	 */ 
	private $loginView;

	/**
	 * @param String  
	 */ 
	private $titel;	

	/**
	 * @param \mysqli $mysqli
	 */
	public function __construct(\mysqli $mysqli){

		$this->loginView = new \view\LoginView();
		$userAgent = $this->loginView->getUserAgent();
		$ip = $this->loginView->getIp();

		$userDAL = new \model\UserDAL($mysqli);
		$userSaver = new \model\UserSaver();
		$this->loginModel = new \model\LoginModel($userSaver, 
												  $userAgent, 
												  $ip, 
												  $userDAL, 
												  $this->loginView);
	
		try{	
			$userCredentials = $this->loginView->getCookieUserCredentials();
			$this->loginModel->setUser($userCredentials); 
		}catch(\Exception $exception){}

		$this->loginController = new LoginController($this->loginView,
													 $userSaver,
													 $userDAL,
													 $this->loginModel);
	}

	/**
	 * @return String HTML
	 */ 
	public function doApplication(){
		$HTML;
		$newLogin = false;
		$this->titel = $this->loginView->signedOutTitel();
		$isLoggedin = $this->loginModel->isLoggedin($newLogin);

		if(!$isLoggedin){
			$this->loginController->login();
		}
		$newLogin = true;
		if($isLoggedin || $this->loginModel->isLoggedin($newLogin)){
			$userCredentials = $this->loginModel->getUserCredentials();
			if(!$this->loginView->userLogsout()){
				$this->titel = $this->loginView->signedInTitel();
				$generateNewPass = $this->loginModel->generateNewPass();
				if(!$isLoggedin){
					$generateNewPass = true;
				}
				$HTML = $this->loginController->stayLoggedin($userCredentials,
															 $generateNewPass);
			}else{
				$HTML = $this->loginController->logout($userCredentials);
			}
		}else{
			$HTML = $this->loginView->getLoginForm();
		}
		return $HTML;
	}

	/**
	 * @return String Titel
	 */ 
	public function getTitel(){
		return $this->titel;
	}
}