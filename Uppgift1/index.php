<?php

require_once("./src/view/HTMLPage.php");
require_once("./src/controller/Login.php");
require_once("./src/model/UserSaver.php");
require_once("./src/model/DateTime.php");

session_start();

$mysqli = new mysqli("mysql08.citynetwork.se", 
					 "119294-ap62823", 
					 "Password123", 
					 "119294-labbar");

$loginController = new \controller\Login($mysqli);

$UserSaver = new \model\UserSaver();
try{
	$loginController->user = $UserSaver->getUser();
}catch(Exception $exception){}

$content = $loginController->authorise();
$titel = $loginController->titel;

$signedIn = $loginController->signedIn;

if($signedIn){
	$user = $loginController->user;
	$UserSaver->setUser($user);
}else if(!$loginController->saveUser){
	$UserSaver->removeUser();
}

$dateTime = new \model\DateTime();

$content .= $dateTime->getDate();

$pageView = new \view\HTMLPage();

echo $pageView->getPage("Laboration $titel", $content);

$mysqli->close();