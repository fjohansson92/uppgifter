<?php

namespace view;

class SignedIn{

	/**
	 * User created from input.
	 * @var \model\User
	 */
	private $user;

	/**
	 * @var boolean
	 */
	public $showSuccessMessage;

	/**
	 * @var  String
	 */ 
	public $message;

	/**
	 * @param \model\User
	 */
	public function __construct(\model\User $user){

		$this->user = $user;
	}

	/**
	 * Return HTML to present when logged in.
	 * @return String HTML Content
	 */
	public function getSignedInPage(){

		$UserName = $this->user->getUserName();

		$successMessage = "";
		if($this->showSuccessMessage){
			$successMessage = $this->message;
		}

		return "<h1>Laborationskod fj222dr</h1>
				<h2>$UserName är inloggad</h2>
				<p>$successMessage</p>
				<a href='?logout'>Logga ut</a>";
	}
}
