<?php

namespace view;

class HTMLPage {

	/**
	 * @param  String $titel   
	 * @param  String $content Page content
	 * @return String Final HTML
	 */
	public function getPage($titel, $content){

		return "<!DOCTYPE html>
<html>
	<head>
		<title>$titel</title>
		<meta http-equiv='content-type' content='text/html; charset=utf-8' >
	</head>
	<body>
		$content
	</body>
</html>";
	}
}
