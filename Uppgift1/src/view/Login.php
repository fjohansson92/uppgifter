<?php

namespace view;

require_once("./src/model/User.php");

class Login{

	/**
	 * Location for $_POST where the username exists.
	 * @var String  
	 */
	private static $UserName = "Login::UserName";

	/**
	 * Location for $_POST where the password exists.
	 * @var String  
	 */
	private static $Password = "Login::Password";

	/**
	 * Location for $_POST where the value for if the user 
	 * should be saved exists.
	 * @var String
	 */  
	private static $SaveUser = "Login::SaveUser";

	/**
	 * Location for $_GET when the user tries to login
	 * @var String
	 */
	private static $login = "login";

	/**
	 * Location for $_GET when the user tries to logout
	 * @var String
	 */
	private static $logout = "logout";

	/**
	 * @var String
	 */
	private $message = "Ej Inloggad";

	/** 
	 * Feedback from input
	 * @var String
	 */
	private $feedBackMessage = "";

	/**
	 * Login form
	 * @return String HTML
	 */
	public function getLoginPage(){	
		$userName = $this->getInput(self::$UserName);

		$form = "<form method='post' action='?" . self::$login . 
					"' enctype='multipart/form-data' >
					<fieldset>
						<legend>
							Login - Skriv in användarnamn och lösenord
						</legend>
						<p>$this->feedBackMessage</p>
						<label>Användarnamn:</label>
						<input type='text' name='". self::$UserName .
							"' id='UserNameID' value='$userName' size='20'/>
						<label>Lösenord:</label>
						<input type='password' name='". self::$Password .
							"' id='PasswordID' size='20'/>
						<label>Håll mig inloggad:</label>
						<input type='checkbox' name='". self::$SaveUser .
							"' id='SaveUserID' value='1' />
						<input type='submit' value='Logga in' /> 
					</fieldset>
				</form>";	
		return "<h1>Laborationskod fj222dr</h1>
				<h2>$this->message</h2>
				$form";
	}

	/**
	 * @param  String $input Location for $_POST
	 * @return String Input
	 */
	private function getInput($input){
		if(isset($_POST[$input])){
			return $this->sanitize($_POST[$input]);
		}else{
			return "";
		}
	}

	/**
	 * Sanitize the input
	 * @param  String $input 
	 * @return String 
	 */
	private function sanitize($input){
		$sanitize = trim($input);
		return filter_var($sanitize, FILTER_SANITIZE_STRING);
	}


	/**
	 * Returns if user tries to login
	 * @return boolean
	 */
	public function userLogsin(){
		return isset($_GET[self::$login]);		
	}

	/**
	 * Returns if user wants to logout
	 * @return boolean
	 */
	public function userLogsout(){
		return isset($_GET[self::$logout]);		
	}

	/**
	 * Returns user created from input
	 * @return \model\User
	 */
	public function getUser(){
		$userName = $this->getInput(self::$UserName);
		$password = $this->getInput(self::$Password);

		$saveUser;
		if($this->getInput(self::$SaveUser) == 1){
			$saveUser = true;
		}else{
			$saveUser = false;
		}

		return new \model\User($userName, $password, $saveUser, false);		
	}

    /**
     * Sets feedBackMessage
     * @param String $message Message to display
     */
    public function setfeedBackMessage($message){
    	$this->feedBackMessage = $message;    	
    } 
}

