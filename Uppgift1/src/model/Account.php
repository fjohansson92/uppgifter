<?php

namespace model;

class Account{

	/**
	 * @var String
	 */
	private $userName;

	/**
	 * @var String
	 */
	private $agent;

	/**
	 * @var String Datum Y-m-d H:i:s
	 */					
	private $date;

	/**
	 * @var String
	 */
	private $password;

	/**
	 * @var String
	 */
	private $ip;

	/**
	 * Validate database values.
	 * @param String $userName 
	 * @param String $agent
	 * @param String $date Datum Y-m-d H:i:s
	 * @param String $password
	 * @param String $ip 
	 * @throws Exception if Account wasn't valid.
	 */
	public function __construct($userName, $agent, $date, $password, $ip){

		if($userName == "" || strlen($userName) > 254){  
			throw new \Exception();
		}
		if($agent == "" || strlen($agent) > 254){
			throw new \Exception();
		}
		if($date == "" || 
		   !preg_match('/\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}/',$date)){
			
			throw new \Exception();
		}
		if($password == "" || strlen($password) > 254){
			throw new \Exception();
		}
		if($ip == "" || strlen($ip) > 254){
			throw new \Exception();
		}		

		$this->userName = $userName;
		$this->agent = $agent;
		$this->date = $date;
		$this->password = $password;
		$this->ip = $ip;
	}

	/** 
	 * @return String	
	 */	
	public function getUserName(){
		return $this->userName;		
	}

	/** 
	 * @return String	
	 */
	public function getAgent(){
		return $this->agent;		
	}

	/** 
	 * @return String Datum Y-m-d H:i:s	
	 */
	public function getDate(){
		return $this->date;		
	}

	/** 
	 * @return String	
	 */
	public function getPassword(){
		return $this->password;		
	}

	/** 
	 * @return String	
	 */
	public function getIp(){
		return $this->ip;		
	}
}