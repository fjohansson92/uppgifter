<?php

namespace model;

require_once("./src/model/Account.php");

class AccountDAL{

	/**
	 * @var String 
	 */
	private static $tableName = "accounts";

	/**
	 * @var \mysqli
	 */ 
	private $mysqli;

	/**
	 * @param \mysqli $mysqli 
	 */ 
	public function __construct(\mysqli $mysqli){

		$this->mysqli = $mysqli;
	}

	/**
	 * Get saved account from database.
	 * @param  String $userName 
	 * @param  String $password
	 * @return \model\Account 
	 */  
	public function getAccount($userName, $password){
		
		$sql = "SELECT userName, agent, date, password, ip
				FROM " . self::$tableName . "
				WHERE userName = '" . $userName . "' AND
					  password = '" . $password . "';";
	
		$result = $this->testMysqli($sql);  
		$account;
		while ($object = $result->fetch_array(MYSQLI_ASSOC)){
			
			$account = new Account($object["userName"],
								   $object["agent"],
								   $object["date"],
								   $object["password"],
								   $object["ip"]);
		}

		if(isset($account)){
			return $account;
		}
		throw new \Exception();
	}

	/**
	 * Save account in database.
	 * @param \model\Account $account
	 */ 
	public function saveAuthorization(Account $account){
		
		$sql = "INSERT INTO " . self::$tableName . "
				(
					userName,
					agent,
					date,
					password,
					ip
				)
				VALUES(?, ?, ?, ?, ?)";

		$statement = $this->mysqli->prepare($sql);
		if($statement === FALSE){
			throw new \Exception();
		}
		if ($statement->bind_param("sssss",$account->getUserName(),			
										   $account->getAgent(),
										   $account->getDate(),
										   $account->getPassword(),
										   $account->getIp()) === FALSE){
			throw new \Exception();
		}
		if ($statement->execute() === FALSE) {
			throw new \Exception();
		}
	}	

	/**
	 * Remove account from database and invalid ones.
	 * @param  String $userName
	 * @param  String $password
	 * @param  String $date Y-m-d H:i:s
	 */  
	public function cleanDatabase($userName, $password, $date){

		$sql = "DELETE FROM " . self::$tableName . "
				WHERE userName = '" . $userName . "' AND  
					  password = '" . $password . "' OR
					  date < '" . $date . "';"; 

		if ($this->mysqli->query($sql) === FALSE) {
			throw new \Exception();
		}
	}

	/**
	 * @param  String $sql 
	 * @return \mysqli_result 
	 */ 
	private function testMysqli($sql){

		$statement = $this->mysqli->prepare($sql);
		if($statement === FALSE){
			throw new \Exception();
		}

		if ($statement->execute() === FALSE) {
			throw new \Exception();
		}

		return $statement->get_result();
	}
}