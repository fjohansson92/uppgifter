<?php

namespace model;

class DateTime{

	/**
	 * Weekdays in Swedish
	 * @var array
	 */
	private $day = array("Måndag", "Tisdag", "Onsdag", "Torsdag", "Fredag", 
		"Lördag", "Söndag");

	/**
	 * Months in Swedish
	 * @var array
	 */
	private $month = array("Januari", "Februari", "Mars", "April", "Maj","Juni",
	 	"Juli", "Augusti", "September", "Oktober", "November", "December");

	/**
	 * @return String HTML
	 */
	public function getDate(){

		return "<p>" . 
			$this->day[date("N") - 1] . ", den " . date("j") . " " . 
			$this->month[date("n") - 1] . " år " . date("Y") . 
			". Klockan är [" . date("G") . ":" . date("i") . ":" . date("s") . 
			"].</p>";
	}

}
