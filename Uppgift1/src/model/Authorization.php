<?php

namespace model;

class Authorization{

	/**
	 * User created from input.
	 * @var \model\User
	 */
	private $user;

	/**
	 * @param \model\User
	 */
	public function __construct(\model\User $user){

		$this->user = $user;
	}

	/**
	 * Return if the input was a valid user.
	 * @return boolean 
	 */
	public function validAccount(){

		if($this->user->getUsername() == "Admin" &&
		  ($this->user->getCreatedByCookie() ||
		   $this->user->getPassword() == "Password")){

			if($this->user->valuesAreSet()){

			   	if($this->validUser()){
			   		return true;
			   	}else{
			   		throw new \Exception();
			   	}
			}else{

				$this->user->setUserAgent();
				$this->user->setIp();

				return true;
			}			   	
		}
		return false;
	}

	/**
	 * @return boolean
	 */ 
	public function validUser(){
		return $this->user->getUserAgent() == $_SERVER['HTTP_USER_AGENT'] &&
			   	   $this->user->getIp() == $_SERVER['SERVER_ADDR'];
	}
}
