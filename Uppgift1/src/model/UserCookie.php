<?php

namespace model;

require_once("./src/model/AccountDAL.php");

class UserCookie{

	/**
	 * Location for cookie where the username exists.
	 * @var String
	 */
	private static $UserCookieLocation = "LoginView::UserName";

	/**
	 * Location for cookie where the password exists.
	 * @var String
	 */	
	private static $PassCookieLocation = "LoginView::Password";

	/**
	 * Format for the date. 
	 * @var String
	 */
	private static $dateFormat = "Y-m-d H:i:s";

	/**
	 * @var \model\AuthorizationDAL
	 * 
	 */ 
	private $authorizationDAL;

	/**
	 * @var  String HTTP_USER_AGENT
	 */ 
	private $userAgent;

	/**
	 * @var String Ip
	 */ 
	private $ip;

	/**
	 * @param \mysqli $mysqli 
	 */ 
	public function __construct(\mysqli $mysqli){

		$this->ip = $_SERVER['SERVER_ADDR'];
		$this->userAgent = $_SERVER['HTTP_USER_AGENT'];
		$this->authorizationDAL = new AccountDAL($mysqli);
	}

	/**
	 * Saves a user in cookies.
	 * @param \model\User $user
	 */
	public function setUser(User $user){

		try{
			$generatedPassword = md5(mcrypt_create_iv(32));
			$time = time()+100;
			$date = date(self::$dateFormat, $time-5);

			$this->removeUser();

			setcookie(self::$UserCookieLocation, $user->getUserName(), $time, "/", ".filipsportfolio.se");
			setcookie(self::$PassCookieLocation, $generatedPassword, $time, "/", ".filipsportfolio.se");	

			$account = new \model\Account($user->getUserName(), 
										  $this->userAgent, 
										  $date, 
										  $generatedPassword,
										  $this->ip);

			$this->authorizationDAL->saveAuthorization($account);

		}catch(\Exception $exception){

			$this->removeUser();
			throw new \Exception();
		}
	}

	/**
	 * Returns User from cookies.
	 * @return \model\User
	 */ 
	public function getAuthrization(){

		try{
			if($this->hasCookie()){

				$userName = $_COOKIE[self::$UserCookieLocation];
				$password = $_COOKIE[self::$PassCookieLocation]; 

				$account = $this->authorizationDAL->getAccount($userName, 
															   $password);

				if($password == $account->getPassword() && 
					date(self::$dateFormat, time()) < $account->getDate() &&
					$this->userAgent == $account->getAgent() &&
					$this->ip == $account->getIp()){

					$user = new \model\User($userName, "", true, true);
					$user->setUserAgent();
					$user->setIp();
					return $user;
				}
			}
			throw new \Exception();
		}catch(\Exception $exception){
			throw new \Exception("Felaktig information i cookie");
		}
	}

	/**
	 * Remove a user from cookie and database.
	 */ 
	public function removeUser(){
		
		try{
			if($this->hasCookie()){
				$userName = $_COOKIE[self::$UserCookieLocation];
				$password = $_COOKIE[self::$PassCookieLocation]; 
				$date = date(self::$dateFormat, time());

				setcookie(self::$UserCookieLocation, "", time()-10000, "/", ".filipsportfolio.se");
				setcookie(self::$PassCookieLocation, "", time()-10000, "/", ".filipsportfolio.se");	

				$this->authorizationDAL->cleanDatabase($userName,
													   $password, 
													   $date);
			}
		}catch(\Exception $Exception){}
	}

	/**
	 * @return boolean 
	 */ 
	public function hasCookie(){

		return isset($_COOKIE[self::$UserCookieLocation]) && 
			   isset($_COOKIE[self::$PassCookieLocation]);
	}
}
