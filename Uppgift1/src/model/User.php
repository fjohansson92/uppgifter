<?php

namespace model;

class User{

	/**
	 * Username for user trying to login.
	 * @var String
	 */
	private $userName;

	/**
	 * Password for user trying to login.
	 * @var String
	 */
	private $password;

	/**
	 * @var boolean
	 */
	private $saveUser;

	/**
	 * @var boolean
	 */
	private $createdByCookie;

	/**
	 * @var String
	 */
	private $userAgent; 

	/**
	 * @var  String
	 */ 
	private $ip;

	/**
	 * Validate username and password.
	 * @param String $UserName Username input
	 * @param String $Password Password input
	 * @param  boolean $saveUser
	 * @param  boolean $createdByCookie
	 * @throws Exception if User couldn't be created.
	 */
	public function __construct($userName, $password, 
								$saveUser, $createdByCookie){

		if($userName == ""){
			throw new \Exception("Användarnamn saknas");
		}
		if($password == "" && !$createdByCookie){
			throw new \Exception("Lösenord saknas");
		}

		$this->userName = $userName;
		$this->saveUser = $saveUser;
		$this->password = $password;
		$this->createdByCookie = $createdByCookie;
	}

	/** 
	 * @return String	
	 */
	public function getUserName(){
		return $this->userName;		
	}

	/**
	 * @return String
	 */
	public function getPassword(){
		return $this->password;		
	}

	/**
	 * @return  boolean
	 */ 
	public function getSaveUser(){
		return $this->saveUser;
	}

	/**
	 * @return  boolean
	 */ 
	public function getCreatedByCookie(){
		return $this->createdByCookie;
	}

	/**
	 * @return String
	 */
	public function getUserAgent(){
		return $this->userAgent;
	}

	/**
	 * @return String
	 */
	public function getIp(){
		return $this->ip;
	}

	public function setUserAgent(){
		$this->userAgent = $_SERVER['HTTP_USER_AGENT'];
	}

	public function setIp(){
		$this->ip = $_SERVER['SERVER_ADDR'];
	}

	/**
	 * @return  boolean
	 */ 
	public function valuesAreSet(){

		return isset($this->userAgent) &&
			   isset($this->ip);
	}

}
