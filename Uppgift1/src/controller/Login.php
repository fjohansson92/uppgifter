<?php

namespace controller;

require_once("./src/view/Login.php");
require_once("./src/model/Authorization.php");
require_once("./src/view/SignedIn.php");
require_once("./src/model/UserCookie.php");

class Login{

	/**
	 * @var \view\Login
	 */
	private $loginView;

	/**
	 * @var String
	 */
	public $titel;

	/**
	 * @var \model\User
	 */
	public $user;

	/**
	 * @var boolean
	 */
	public $signedIn;

	/**
	 * @var \view\SignedIn
	 */
	private $signedInView;

	/**
	 * @var \model\UserCookie
	 */
	private $userCookie;

	/**
	 * @var boolean
	 */ 
	public 	$saveUser;

	/**
	 * @var boolean
	 */
	private $newLogin;

	/**
	 * @var boolean
	 */
	private $authorizedByCookie;

	/**
	 * @var boolean
	 */
	private $showSuccessMessage;

	public function __construct(\mysqli $mysqli){

		$this->loginView = new \view\Login();
		$this->mysqli = $mysqli;
		$this->userCookie = new \model\UserCookie($this->mysqli);
	}

	/**
	 * @return String HTML Content
	 */
	public function authorise(){

		// If user tries to login or is loggedin since before.	
		if($this->loginView->userLogsin() || 
		   $this->userCookie->hasCookie() && 
		   !$this->loginView->userLogsout() ||
		   isset($this->user) && !$this->loginView->userLogsout()){

			if($this->login()){
				return $this->signedInView->getSignedInPage();
			}
		}else if($this->loginView->userLogsout()){
			$this->logout();
		}

		$this->signedIn = false;
		$this->titel = "Inte inloggad";		
		return $this->loginView->getLoginPage();
	}

	/**
	 * Handles when user tries to login.
	 * @return String HTML Content
	 */
	private function login(){
		try{
			if(!isset($this->user)){
				$this->getUser();
			}
			$accountAuthorization = new \model\Authorization($this->user);
			$validAccount;
			try{
				$validAccount = $accountAuthorization->validAccount();
			}catch(\Exception $Exception){
				$this->saveUser = true;
				throw new \Exception();
			}
			if($validAccount){	
				$this->doLogin();
			}else{
				$this->loginView->setfeedBackMessage(
					"Felaktigt användarnamn och/eller lösenord");
			}
		}catch(\Exception $exception){
			$this->loginView->setfeedBackMessage($exception->getMessage());
			try{
				$this->user = $this->userCookie->removeUser();		
			}catch(\Exception $exception){}
		}
		return $this->signedIn;
	}

	/**
	 * Gets and sets user from cookies or input.
	 */ 
	private function getUser(){

		if($this->userCookie->hasCookie()){
			$this->user = $this->userCookie->getAuthrization();
			$this->newLogin = true;	
			$this->authorizedByCookie = true;
		}else{
			$this->user = $this->loginView->getUser();
			$this->showSuccessMessage = true;
			$this->newLogin = true;
		}
	}


	/**
	 * Handles a successful login.
	 */ 
	private function doLogin(){

		$databaseFail = false;

		if($this->user->getSaveUser() && $this->newLogin){

			try{
				$this->userCookie->setUser($this->user);
			}catch(\Exception $exception){
				$databaseFail = true;
			}
		}

		$this->signedIn = true;				
		$this->titel = "Inloggad";

		$this->signedInView = new \view\SignedIn($this->user);	
		$this->signedInView->showSuccessMessage = $this->showSuccessMessage;
		$this->setMessage($databaseFail);
	}

	/**
	 * @param boolean $databaseFail
	 */ 
	private function setMessage($databaseFail){

		if($databaseFail){
			$this->signedInView->message = 
				"Inloggning lyckades men det gick inte att spara cookies";	
		}else if($this->authorizedByCookie){
			$this->signedInView->showSuccessMessage = true;
			$this->signedInView->message = "Inloggning lyckades via cookies";
		}else if($this->user->getSaveUser()){
			$this->signedInView->message = 
				"Inloggning lyckades och vi kommer ihåg dig nästa gång";
		}else{
			$this->signedInView->message = "Inloggning lyckades";
		}
	}


	/**
	 * Handles when user tries to logout.
	 */
	private function logout(){

		if(isset($this->user)){
			$accountAuthorization = new \model\Authorization($this->user);

			if($accountAuthorization->validUser()){
				$this->user = $this->userCookie->removeUser();
				$this->loginView->setfeedBackMessage("Du har nu loggat ut");
			}else{
				$this->saveUser = true;
			}
		}	
		$this->signedIn = false;
	}
}
