<?php

namespace login\view;

class RegisterView{

	private static $REGISTER = "register";	
	private static $USERNAME = "LoginView::UserName";
	private static $PASSWORD = "RegisterView::Password";
	private static $REPEATEDPASSWORD = "RegisterView::RepeatedPassword";

	private $message = "";

	public function getRegisterButton() {
		return  "<a href='?" . self::$REGISTER . "'>Registrera ny användare</a>";
	}

	public function getBackButton() {
		return  "<a href='?''>Tillbaka</a>";
	}

	public function isGoingToRegister(){
		return isset($_GET[self::$REGISTER]);
	}

	public function getRegisterBox(){
		$userName = $this->getUserName();

		$html = "
			<form action='?" . self::$REGISTER . "' method='post' enctype='multipart/form-data'>
				<fieldset>
					$this->message
					<legend>Registrera ny användare - Skriv in användarnamn och lösenord</legend>
					<label for='UserNameID' >Användarnamn :</label>
					<input type='text' size='20' name='" . self::$USERNAME . "' id='UserNameID' value='$userName'/>
					<label for='PasswordID' >Lösenord  :</label>
					<input type='password' size='20' name='" . self::$PASSWORD . "' id='PasswordID' value='' />
					<label for='repeatedPasswordID' >Lösenord  :</label>
					<input type='password' size='20' name='" . self::$REPEATEDPASSWORD . "' id='repeatedPasswordID' value='' />
					<label for 'submit'>Skicka :</label>
					<input type='submit' name=''  value='Registrera' id='submit'/>
				</fieldset>
			</form>";
			
		return $html;
	}

	public function registerFailed() {		
		
		$userName;
		$password;
		$repeatedPassword;

		try{
			if ($this->getUserName() == "") {
				throw new \Exception();
			} else if($this->userNameHasTags()) {
				$this->message .= "<p>Användarnamnet innehåller ogiltliga tecken.</p>";
			} else{
				$userName = new \login\model\UserName($this->getUserName());
			}
		}catch(\Exception $e){
			$this->message .= "<p>Användarnamnet har för få tecken. Minst 3 tecken</p>";
		}

		try{
			if ($this->getPassword() == "") {
				throw new \Exception();
			} else {
				$password = \login\model\Password::fromCleartext($this->getPassword());
			}
		}catch(\Exception $e){
			$this->message .= "<p>Lösenordet har för få tecken. Minst 6 tecken</p>";
		}

		if(isset($userName) && isset($password)){
			try{
				$repeatedPassword = \login\model\Password::fromCleartext($this->getRepeatedPassword());
				$password->compareEncryptedString($repeatedPassword);
			}catch(\Exception $e){
				$this->message .= "<p>Lösenord matchar inte</p>";
			}
		}

		if($this->message == ""){

			$this->message .= "<p>Användarnamnet är redan upptaget</p>";
		}
	}

	public function regesteringUser(){
		return isset($_POST[self::$USERNAME]);
	}


	public function getUser(){
		
		if ($this->userNameHasTags()) {
			throw new \Exception();
		}

		$userName = new \login\model\UserName($this->getUserName());
		$password = \login\model\Password::fromCleartext($this->getPassword());
		$repeatedPassword = \login\model\Password::fromCleartext($this->getRepeatedPassword());
		$password->compareEncryptedString($repeatedPassword);

		return \login\model\UserCredentials::createFromClientData($userName, $password);
	}


	private function getUserName() {
	if (isset($_POST[self::$USERNAME])){	
		return \Common\Filter::sanitizeString($_POST[self::$USERNAME]);
	}else
		return "";
	}

	private function userNameHasTags() {
		return \Common\Filter::hasTags($_POST[self::$USERNAME]); 
	}

	private function getPassword() {
		if (isset($_POST[self::$PASSWORD]))
			return \Common\Filter::sanitizeString($_POST[self::$PASSWORD]);
		else
			return "";
	}

	private function getRepeatedPassword() {
		if (isset($_POST[self::$REPEATEDPASSWORD]))
			return \Common\Filter::sanitizeString($_POST[self::$REPEATEDPASSWORD]);
		else
			return "";
	}
}