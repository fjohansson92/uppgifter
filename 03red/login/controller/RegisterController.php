<?php

namespace login\controller;

require_once("./login/model/RegisterModel.php");

class RegisterController {
	
	private $registerView;
	private $registerModel;

	private $userRegistred = false;

	public function __construct(\login\view\RegisterView $registerView){

		$this->registerView = $registerView;
		$this->registerModel = new \login\model\RegisterModel();
	}

	public function registred(){

		return $this->userRegistred;
	}

	public function isGoingToRegister(){
		return $this->registerView->isGoingToRegister() && !$this->userRegistred;
	}

	public function doToggleRegister(){

		if($this->registerView->regesteringUser()){
			try{

				$this->newUser = $this->registerView->getUser();
				$this->registerModel->doRegister($this->newUser);
				$this->userRegistred = true;
			}catch(\Exception $e){
				$this->registerView->registerFailed();
			}
		}
	}

}