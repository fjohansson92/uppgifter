<?php

namespace login\model;

require_once("UserList.php");

class RegisterModel {

	private $allUsers;	
	

	public function doRegister($newUser) {

		$createUser = true;
		$userName = $newUser->getUserName();
		$this->allUsers = new UserList($userName, $createUser);

		$newUser->newTemporaryPassword();
		$this->allUsers->registerUser($newUser);

	}

}	