<?php

namespace login\model;


require_once("UserCredentials.php");
require_once("common/model/PHPFileStorage.php");

class UserList {

	private $userFile;

	private $users;

	private $newUser = false;

	public function  __construct($userName, $createUser) {
		$this->users = array();
		
		$this->loadUser($userName, $createUser);
	}

	public function findUser($fromClient) {
		foreach($this->users as $user) {
			if ($user->isSame($fromClient) ) {
				\Debug::log("found User");
				return  $user;
			}
		}
		throw new \Exception("could not login, no matching user");
	}

	public function registerUser($newUser){

		if (!$this->newUser) {
			throw new \Exception();
		}

		$this->userFile->writeItem($newUser->getUserName(), $newUser->toString());
	}


	public function update($changedUser) {
		$this->userFile->writeItem($changedUser->getUserName(), $changedUser->toString());

		\Debug::log("wrote changed user to file", true, $changedUser);
		$this->users[$changedUser->getUserName()->__toString()] = $changedUser;
	}

	private function loadUser($userName, $createUser) {

		$fileName = strtolower($userName);
		if(!file_exists("data/$fileName.php")){
			$this->newUser = true;
		}

		if($createUser || !$this->newUser) {
			$this->userFile = new \common\model\PHPFileStorage("data/$fileName.php");
			try {
				$userString = $this->userFile->readItem($userName);
				$user = UserCredentials::fromString($userString);

				$this->users[$user->getUserName()->__toString()] = $user;

			} catch (\Exception $e) {}
		}	
	}
}