<?php

namespace application\view;

require_once("common/view/Page.php");
require_once("SwedishDateTimeView.php");



class View {
	private $loginView;

	private $timeView;
	
	private $registerView;

	public function __construct($loginView,\login\view\RegisterView $registerView) {
		$this->loginView = $loginView;
		$this->timeView = new SwedishDateTimeView();
		$this->registerView = $registerView;
	}
	
	public function getLoggedOutPage() {
		$html = $this->getHeader(false);

		$registerButton = $this->registerView->getRegisterButton();
		$loginBox = $this->loginView->getLoginBox(); 

		$html .= "$registerButton
				  <h2>Ej Inloggad</h2>
				  $loginBox
				 ";
		$html .= $this->getFooter();

		return new \common\view\Page("Laboration. Inte inloggad", $html);
	}
	
	public function getLoggedInPage($user) {
		$html = $this->getHeader(true);
		$logoutButton = $this->loginView->getLogoutButton(); 
		$userName = $user->getUserName();

		$html .= "
				<h2>$userName är inloggad</h2>
				 	$logoutButton
				 ";
		$html .= $this->getFooter();

		return new \common\view\Page("Laboration. Inloggad", $html);
	}
	
	public function getRegisterPage() {
		$html = $this->getHeader(true);

		$backButton = $this->registerView->getBackButton();
		$registerBox = $this->registerView->getRegisterBox();

		$html .= "$backButton
				  <h2>Ej inloggad, Registrera användare</h2>
				  $registerBox";

		$html .= $this->getFooter();

		return new \common\view\Page("Registrering av ny användare", $html);
	}

	private function getHeader($isLoggedIn) {
		$ret =  "<h1>Laborationskod xx222aa</h1>";
		return $ret;
		
	}

	public function getRegistredPage() {

		$this->loginView->RegisterOk();
		return $this->getLoggedOutPage();
	}

	private function getFooter() {
		$timeString = $this->timeView->getTimeString(time());
		return "<p>$timeString<p>";
	}
	
	
	
}
