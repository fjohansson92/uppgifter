<?php

namespace application\controller;

require_once("application/view/View.php");
require_once("login/controller/LoginController.php");
require_once("login/controller/RegisterController.php");
require_once("login/view/RegisterView.php");

class Application {
	private $view;

	private $loginController;

	private $registerController;
	
	public function __construct() {
		$loginView = new \login\view\LoginView();
		$registerView = new \login\view\RegisterView();
		
		$this->registerController = new \login\controller\RegisterController($registerView);
		$this->loginController = new \login\controller\LoginController($loginView);
		$this->view = new \application\view\View($loginView, $registerView);
	}
	
	public function doFrontPage() {
		$this->loginController->doToggleLogin();
		$this->registerController->doToggleRegister();
	
		if ($this->loginController->isLoggedIn()) {
			$loggedInUserCredentials = $this->loginController->getLoggedInUser();
			return $this->view->getLoggedInPage($loggedInUserCredentials);	
		} else if($this->registerController->isGoingToRegister()){
			return $this->view->getRegisterPage();
		} else if($this->registerController->registred()) {
			return $this->view->getRegistredPage();
		} else {
			return $this->view->getLoggedOutPage();
		}
	}
}
