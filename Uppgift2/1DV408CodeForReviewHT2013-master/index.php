<?php

session_start();

require_once('LikeController.php');
require_once('PageView.php');

class MasterController {

	public static function doControll() {
		//TODO: Show that this is a controller
		$likeController = new LikeController();
		
		$html = $likeController->doControll();
		
		//TODO: Use Common/PageView
		$pageView = new \Common\PageView();
		
		return $pageView->GetHTMLPage("I like titles", $html);
	}
}

echo MasterController::doControll();

//	Code-Review kommentarer
//	En klass per fil och filnamn är klassnamn 
//		Filen har inte samma namn som klassen.
//	Alla returvärden är dokumenterade
//		Finns ingen dokumentation för returvärdet för doControll.
//	Ogiltiga kommentarer 
//		TODO kommentaren ska inte vara kvar.