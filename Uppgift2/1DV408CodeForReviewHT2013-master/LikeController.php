<?php

require_once('LikeModel.php');
require_once('LikeView.php');

class LikeController {
	
	/**
	 * hantera en knapptryckning
	 * visa en like knapp
	 * visa hur många likes
	 * @return String HTML
	 */
	public function doControll() {
		$likeView = new LikeView();
		$likeModel = new LikeModel();
		
		//Handle input and collect messages
		$message = LikeView::NO_MESSAGE;
		if ($likeView->didUserLike()) {
			$likeModel->userDoLike();

			//TODO: This message should be abstract here (const)
			//and translated in view...
			$message = LikeView::USER_DID_LIKE;
		}
		
		//Collect possibly changed state
		$likes = $likeModel->getNumberOfLikes();
		$userHasLiked = $likeModel->userHasLiked();
		
		//Create output and return
		return $likeView->doOutput($likes, $userHasLiked, $message);
	}
}

//	Code-Review kommentarer
//	Metoderna gör det de heter 
//		Metoden doControll skulle kunna ha ett mer beskrivande namn
//	Medlemsvariabler och lokala variabler har talande namn
//		Variablen message innehåller egentligen inte själv meddelandet och skulle därför 
//		kunna ha ett namn som mer förtydligar dess uppgift.
//	Endast ett språk 
//		Finns kommentarer på svenska.
//	Ogiltiga kommentarer 
//		TODO kommentaren ska inte vara kvar.